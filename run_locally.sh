#!/bin/bash
docker-compose up -d --build

sleep 5
response=$(curl --write-out %{http_code} --silent --output /dev/null localhost:8008/ping)
if [ ${response} == "200" ]; then
    echo -e "\nServer is up and running at http://localhost:8008/people"
fi