# Kubernetes manifests
The following describes the setup of the kubernetes manfiests created for the exercise, as well as additional notes on how they could be improved. 

## API Backend (Go)
The manifests for this are very simple by design, as I did not know what limitations would be in place on the host machines running this. Much can be improved upon and added given the context of the application and the running environment.



### Deployment
* Creates two running pods (replicas)
* Has a mix of environmental data set both inline and also retrieved from a Kubernetes secret.
* Contains basic readiness/liveness probes which talk to a custom ping endpoint on the backend

#### Potential improvements
When deploying applications to a production cluster correct `requests` and `limits` should be added to keep the cluster stable. These ensure an application does not take more than it needs, but also has enough resources to do it's job, more importantly, this also ensures that other running applications do not get starved of resources. Ideally these resource requests and limits would be driven by detailed load testing.

As this application did not require many environment variables I decided to put them inline, however I am a fan of driving environment data via config maps.


### Service and Ingress
* Creates a simple NodePort service.

#### Potential improvements
A Kubernetes service has a lot that can be configured depending on the context and environment. For this exercise I used minikube locally so a complex service was not needed.

I used an Ingress controller for this exercise to expose an external IP. If I was deploying to a cloud provider I would have opted to use a vendor specific load balanced solution to route traffic, or perhaps would have adjusted the service to be of type `LoadBalancer` to automate some of the work.


## MongoDB
I did not have a lot of experience working with Databases within Kubernetes so I wanted to keep the scope of this solution quite minimal. I have since read many articles about the optimal way to approach databases within Kubernetes, specifically with mongo, which detail the benefits of using `StatefulSets`.

A lot of improvements can be made to my solution. In a production environment a `PersistentVolume` and matching `PersistentVolumeClaim` would be defined similar to the following:

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: mongo-pv
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 5Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/data/db"
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mongo-pv-claim
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 3Gi
```