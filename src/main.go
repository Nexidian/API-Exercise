package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Person struct to hold the API definition
type Person struct {
	Survived                ConvertibleBoolean `json:"survived" bson:"survived"`
	Name                    string             `json:"name" bson:"name"`
	PassengerClass          int                `json:"passengerClass" bson:"passengerClass"`
	Sex                     string             `json:"sex" bson:"sex"`
	Age                     float64            `json:"age" bson:"age"`
	SiblingsOrSpousesAboard int                `json:"siblingsOrSpousesAboard" bson:"siblingsOrSpousesAboard"`
	ParentsOrChildrenAboard int                `json:"parentsOrChildrenAboard" bson:"parentsOrChildrenAboard"`
	Fare                    float64            `json:"fare" bson:"fare"`
	Uuid                    primitive.ObjectID `json:"uuid" bson:"_id"`
}

type ConvertibleBoolean bool

// ConvertibleBoolean courtesy of https://stackoverflow.com/questions/30856454/how-to-unmarshall-both-0-and-false-as-bool-from-json
func (bit ConvertibleBoolean) UnmarshalJSON(data []byte) error {
	asString := string(data)
	if asString == "1" || asString == "true" {
		bit = true
	} else if asString == "0" || asString == "false" {
		bit = false
	} else {
		return errors.New(fmt.Sprintf("Boolean unmarshal error: invalid input %s", asString))
	}
	return nil
}

// Return the full list of people
func getPeople(w http.ResponseWriter, r *http.Request) {
	var people []*Person
	dbConnection := GetDBConnection()
	collection := GetCollection(dbConnection)

	cur, err := collection.Find(context.TODO(), bson.M{})
	if err != nil {
		log.Fatal("Error Finding documents: ", err)
		WriteFailure(w)
		return
	}

	// Iterate over the responses from the prevous .Find()
	for cur.Next(context.TODO()) {
		// Create a new instance of our Person object and assign the values based on the dataset from the DB
		var singlePerson Person
		err = cur.Decode(&singlePerson)
		if err != nil {
			log.Fatal("Error Decoding the document: ", err)
			WriteFailure(w)
			return
		}

		people = append(people, &singlePerson)
	}

	var responseJson string
	// Iterate over the returned dataset and create our JSON response
	for _, person := range people {
		personJSON, err := json.Marshal(person)
		if err != nil {
			fmt.Println("Could not correctly marshal the struct into JSON: ", err)
			WriteFailure(w)
			return
		}

		responseJson = responseJson + string(personJSON) + ","
	}

	WriteSuccess(w)
	w.Write([]byte(responseJson))
}

func getPerson(w http.ResponseWriter, r *http.Request) {
	dbConnection := GetDBConnection()
	collection := GetCollection(dbConnection)

	pathParams := mux.Vars(r)

	if uuid, ok := pathParams["uuid"]; ok {
		var person Person
		objID, _ := primitive.ObjectIDFromHex(uuid)
		documentReturned := collection.FindOne(context.TODO(), bson.M{"_id": objID})
		documentReturned.Decode(&person)

		responseJSON, err := json.Marshal(person)
		if err != nil {
			fmt.Println("Could not correctly marshal the struct into JSON: ", err)
			WriteFailure(w)
			return
		}

		WriteSuccess(w)
		w.Write([]byte(string(responseJSON)))
	}
}

func addPerson(w http.ResponseWriter, r *http.Request) {
	dbConnection := GetDBConnection()
	collection := GetCollection(dbConnection)

	decoder := json.NewDecoder(r.Body)
	var newPerson Person
	err := decoder.Decode(&newPerson)

	if err != nil {
		log.Fatal("could not decode payload: ", err)
		WriteFailure(w)
		return
	}

	// Create a new UUID
	newPerson.Uuid = primitive.NewObjectID()

	// Insert the new person
	insertResults, err := collection.InsertOne(context.TODO(), newPerson)
	if err != nil {
		log.Fatal("Failed to insert a new person: ", err)
		WriteFailure(w)
		return
	}

	fmt.Println("Inserted a single document: ", insertResults.InsertedID)

	responseJSON, err := json.Marshal(newPerson)
	if err != nil {
		fmt.Println("Could not correctly marshal the struct into JSON: ", err)
	}

	WriteSuccess(w)
	w.Write([]byte(responseJSON))
}

func updatePerson(w http.ResponseWriter, r *http.Request) {
	pathParams := mux.Vars(r)

	var arbitrary_json map[string]interface{}
	dbConnection := GetDBConnection()
	collection := GetCollection(dbConnection)

	if uuid, ok := pathParams["uuid"]; ok {
		objID, _ := primitive.ObjectIDFromHex(uuid)

		// Read in raw body for processing
		requestJSON, _ := ioutil.ReadAll(r.Body)
		json.Unmarshal([]byte(requestJSON), &arbitrary_json)

		fmt.Println(arbitrary_json)

		var fieldsToUpdate bson.D

		if survived, ok := arbitrary_json["survived"]; ok {
			fieldsToUpdate = append(fieldsToUpdate, bson.E{"survived", survived})
		}

		if passengerClass, ok := arbitrary_json["passengerClass"]; ok {
			fieldsToUpdate = append(fieldsToUpdate, bson.E{"passengerClass", passengerClass})
		}

		if name, ok := arbitrary_json["name"]; ok {
			fieldsToUpdate = append(fieldsToUpdate, bson.E{"name", name})
		}

		if sex, ok := arbitrary_json["sex"]; ok {
			fieldsToUpdate = append(fieldsToUpdate, bson.E{"sex", sex})
		}

		if age, ok := arbitrary_json["age"]; ok {
			fieldsToUpdate = append(fieldsToUpdate, bson.E{"age", age})
		}

		if siblingsOrSpousesAboard, ok := arbitrary_json["siblingsOrSpousesAboard"]; ok {
			fieldsToUpdate = append(fieldsToUpdate, bson.E{"siblingsOrSpousesAboard", siblingsOrSpousesAboard})
		}

		if parentsOrChildrenAboard, ok := arbitrary_json["parentsOrChildrenAboard"]; ok {
			fieldsToUpdate = append(fieldsToUpdate, bson.E{"parentsOrChildrenAboard", parentsOrChildrenAboard})
		}

		if fare, ok := arbitrary_json["fare"]; ok {
			fieldsToUpdate = append(fieldsToUpdate, bson.E{"fare", fare})
		}

		setMap := bson.D{
			{"$set", fieldsToUpdate},
		}

		_, err := collection.UpdateOne(context.TODO(), bson.M{"_id": objID}, setMap)
		if err != nil {
			log.Fatal("Error updating person", err)
			WriteFailure(w)
			return
		}

		WriteSuccess(w)
	}
}

func deletePerson(w http.ResponseWriter, r *http.Request) {
	dbConnection := GetDBConnection()
	collection := GetCollection(dbConnection)

	pathParams := mux.Vars(r)

	if uuid, ok := pathParams["uuid"]; ok {
		objID, _ := primitive.ObjectIDFromHex(uuid)
		deleteRequest, err := collection.DeleteOne(context.TODO(), bson.M{"_id": objID})

		if err != nil {
			log.Fatal("An error occured when trying to delete a person: ", err)
			w.Header().Set("Content-type", "application/plain")
			w.WriteHeader(http.StatusInternalServerError)
		}

		// Could not find a person to delete with given ID
		if deleteRequest.DeletedCount == 0 {
			w.Header().Set("Content-type", "application/json")
			w.WriteHeader(http.StatusOK)
			w.Write([]byte(fmt.Sprintf(`{"message": "Could not find person with UUID: %s"}`, uuid)))
			return
		}

		WriteSuccess(w)
	}
}

func invalidOrNotFound(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusAccepted)
	w.Write([]byte(`{"message": "Either you have sent an invalid HTTP verb or we don't not support it, sorry!}`))
}

// Standard success
func WriteSuccess(w http.ResponseWriter) {
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusAccepted)
}

// Standard failure
func WriteFailure(w http.ResponseWriter) {
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
}

func GetDBConnection() *mongo.Client {
	clientOptions := options.Client().ApplyURI(os.Getenv("DB_NAME"))

	var auth options.Credential

	auth.AuthSource = os.Getenv("API_DATABASE")
	auth.Username = os.Getenv("API_USER_NAME")
	auth.Password = os.Getenv("API_USER_PASSWORD")

	clientOptions.SetAuth(auth)

	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	return client
}

// Test DB connectivity
func PingDB(w http.ResponseWriter, r *http.Request) {
	dbConnection := GetDBConnection()

	err := dbConnection.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
		w.Header().Set("Content-type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message": "Could not ping the Database."}`))
		return
	}

	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"message": "Successfully pinged the Database."}`))
}

// Return the default collection for the API
func GetCollection(client *mongo.Client) *mongo.Collection {
	return client.Database(os.Getenv("API_DATABASE")).Collection(os.Getenv("API_COLLECTION_NAME"))
}

func main() {
	r := mux.NewRouter()

	// Get a list of all people
	r.HandleFunc("/people", getPeople).Methods(http.MethodGet)
	// Get information about one person
	r.HandleFunc("/people/{uuid}", getPerson).Methods(http.MethodGet)
	// Add a person to the database
	r.HandleFunc("/people", addPerson).Methods(http.MethodPost)
	// Update information about one person
	r.HandleFunc("/people/{uuid}", updatePerson).Methods(http.MethodPut)
	// Delete this person
	r.HandleFunc("/people/{uuid}", deletePerson).Methods(http.MethodDelete)
	// Ping the database connection
	r.HandleFunc("/ping", PingDB).Methods(http.MethodGet)
	// Default path
	r.HandleFunc("/", invalidOrNotFound)

	// Test the db connection
	dbConnection := GetDBConnection()
	err := dbConnection.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")
	fmt.Println("Running server")
	log.Fatal(http.ListenAndServe(":8080", r))
}
