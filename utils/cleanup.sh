#!/usr/bin/env bash

#
# Run this to clean up any mess I created
#
docker-compose down

if [[ ! `docker ps -a -q --filter "name=titanic-api-exercise"` == "" ]]; then
    echo "Removing created containers"
    docker rm $(docker ps -a -q --filter "name=titanic-api-exercise")
fi

