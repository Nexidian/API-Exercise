#!/bin/bash
set -e;

# a default non-root role
MONGO_NON_ROOT_ROLE="${MONGO_NON_ROOT_ROLE:-readWrite}"

# Create a new DB user with credentials driven by environment variables.
# This way we can have a unified creation method for both local environments and kubernetes with the use of config maps for example.
# Note: _js_escape is a function exposed by the upstream docker-entrypoint: https://github.com/docker-library/mongo/blob/b459a2e9f83aac072a9a7a13b6022b7576e4b112/3.6/docker-entrypoint.sh#L140
"${mongo[@]}" "$MONGO_INITDB_DATABASE" <<-EOJS
    db = db.getSiblingDB($(_js_escape "$API_DATABASE"))
    db.createUser({
        user: $(_js_escape "$API_USER_NAME"),
        pwd: $(_js_escape "$API_USER_PASSWORD"),
        roles: [ { role: $(_js_escape "$API_USER_DB_ROLE"), db: $(_js_escape "$API_DATABASE") } ]
        })
EOJS

# I'm using mongoimport here to simplify the import process. 
# If we needed to have control over what is imported and/or how, I would move this to seperate script
# which would get called here instead, for example ./import_dataset.py
mongoimport -d $API_DATABASE -c $API_COLLECTION_NAME --type csv --headerline  --file /titanic.csv 