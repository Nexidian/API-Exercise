# Introduction
For this exercise I've decided to implement a Golang API with MongoDB, mostly because I wanted to try something a little different than what I would normally use. I saw this as a good opportunity to step outside
my comfort zone a little.

# Installation
I've provided solutions to run both locally with Docker and also within Kubernetes (Minikube)

## Local
I've written two dockerfiles for local use `/dockerfiles/application.Dockerfile` and `/dockerfiles/mongo.Dockerfile`. These are tied together with `docker-compose.yaml`.


To run locally you can run `docker-compose up -d --build` from the root directory of this project and then visit the following URL when complete.

```bash
http://localhost:8008/people
```

Alternatively you can run the small start up script:


```bash
bash run_locally.sh
```
This will run `docker compose` and let you know when the server is up.


## Kubernetes (Minikube)
For information on these manifests please read the [README](../kubernetes/README.md) found within the `/kubernetes` folder.

I've pre-built the required images to my dockerhub, they are named `nexidian/mongo-api-db` and `nexidian/goapi`

### Deploying 
I used Minikube for testing my solution locally. All of the manifests can be found within `/kubernetes`.

Apply these to the cluster by running the follow command from the root directory of this project
```bash
kubernetes apply -f kubernetes/
```

**_Note: You may have to wait for a minute or two for the ingress controller to assign you a external IP address. Check for the IP address with `kubectl get ing`_**

---

# API Backend
The API has been written in Go and is contained to a single file called `main.go`. I have utilized a multistage dockerfile here to vastly reduce the final size of the built image.

The first stage pulls from the upstream Go image, sets up some environment variables and then compiles application. The compilation is done entirely within Docker so there is no need to have dependencies installed on the host machine. 

The second and last stage copies the compiled application from the previous stage and places it into a `scratch` image. This reduces the size of the image from `~360MB` to `~15mb`.

This scratch image is very limited however, and would not suit an environment where you would want to be able to connect to the running container and run commands. You could replace the scratch image with a generic Alpine image if you wanted to have some extra level of functionality.

I've used the following third party libraries:
* [Gorrila Mux](https://github.com/gorilla/mux) - I used Gorilla for the request router/dispatcher. It's simplicity made it a great choice to help get off of the ground. 
* [MongoDB Go Driver](https://github.com/mongodb/mongo-go-driver) - This was an obvious choice for interfacing with MongoDb

### Functionality
The entry point for the application can be found within `main()`. Here I set up the `Mux` router and define the API endpoints. 

Take the following for example:

```Go
func main() {
	r := mux.NewRouter()

	// Get a list of all people
	r.HandleFunc("/people", getPeople).Methods(http.MethodGet)
	
    ...
}
```

Here we instantiate a new router object and define a new route of `/people` and bind it to the function `getPeople`. This tells the router to listen for requests to that path, and call the given function when needed.

If we assume our application is running locally the URI would look like `http://localhost:8080/people`.

The `Methods(http.MethodGet)` part lets the router know to only route traffic if the HTTP verb matches what we've set. This allows us
to have greater control over how our traffic is routed. This also allows us to have the same endpoint respond to different types of requests

For example take the following: 

```Go
	r.HandleFunc("/people", getPeople).Methods(http.MethodGet)
	r.HandleFunc("/people", addPerson).Methods(http.MethodPost)
```

We have defined two routes, both to the same endpoint, however one will respond to GET requests, while the other to POST.

---

A struct has been created to hold our data model. This object allows us to have control over how data is retrieved and send to the database, as well as giving providing some passive error handling and structure to the code.

*Note: The API example shows the entry needing to contain a UUID, however they do not exist within the dataset. I have opted to use the insert ID provided by MongoDB and masked it as being the UUID field*

```Go
type Person struct {
	Survived                ConvertibleBoolean `json:"survived" bson:"survived"`
	Name                    string             `json:"name" bson:"name"`
	PassengerClass          int                `json:"passengerClass" bson:"passengerClass"`
	Sex                     string             `json:"sex" bson:"sex"`
	Age                     float64            `json:"age" bson:"age"`
	SiblingsOrSpousesAboard int                `json:"siblingsOrSpousesAboard" bson:"siblingsOrSpousesAboard"`
	ParentsOrChildrenAboard int                `json:"parentsOrChildrenAboard" bson:"parentsOrChildrenAboard"`
	Fare                    float64            `json:"fare" bson:"fare"`
	Uuid                    primitive.ObjectID `json:"uuid" bson:"_id"`
}
```


# Database
For the database I decided to use Mongo for a NoSQL solution. This again was because I wanted to try something that I did not have a ton of experience with.

I used a standard upstream docker image for this with almost no changes to the defaults. 

## Data import
I made use of MongoDB's import function to simplify the import process. If we needed to have control over what is imported and/or how, I would move this to separate script dedicated to parsing the CSV and inserting into Mongo.

The upstream image allows us to supply a init script to the container which is run on creation of the container, but only if Mongos data directory is empty. The init script can be either Javascript or Bash. For this exercise I wrote and provided a shell script which can be found in `/utils/mongo-init.sh`. 

The script is very simple and could be improved on greatly for added functionality. 

I use this init script to create our database and api user credentials:

```bash
"${mongo[@]}" "$MONGO_INITDB_DATABASE" <<-EOJS
    db = db.getSiblingDB($(_js_escape "$API_DATABASE"))
    db.createUser({
        user: $(_js_escape "$API_USER_NAME"),
        pwd: $(_js_escape "$API_USER_PASSWORD"),
        roles: [ { role: $(_js_escape "$API_USER_DB_ROLE"), db: $(_js_escape "$API_DATABASE") } ]
        })
EOJS
```

It is here you can also find the import command, which gets run after our database has been created:

```bash
mongoimport -d $API_DATABASE -c $API_COLLECTION_NAME --type csv --headerline  --file /titanic.csv 
```

The creation of the database and the user are driven by environment variables, this makes it easy to make changes to the variables for use in testing or deploying to different development environments.


